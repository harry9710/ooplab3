package ooplab3;

public abstract class Employee {
	private int salary;
	private String name, surname;
	private String country, city, street;
	private int streetNum;

	// constructor
	public Employee(String name, String surname, int salary) {
		this.name = name;
		this.surname = surname;
		this.salary = salary;
	}

	// method to set address
	public void setAddress(String country, String city, String street, int streetNum) {
		this.country = country;
		this.city = city;
		this.street = street;
		this.streetNum = streetNum;
	}

	// method to calculate the salary
	public int getSalary() {
		return this.salary;
	}

	@Override // method to print basic information
	public String toString() {
		return getClass().getSimpleName() + " - " + name + " " + surname + ", " + country + ", " + city + ", " + street
				+ " " + streetNum + ", salary: " + getSalary() + "\r\n";
	}
}
