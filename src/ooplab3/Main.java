package ooplab3;

public class Main {
	public static void main(String[] args) {
		Employee developer1 = new Developer("name1", "surname1", 100);
		Employee developer2 = new Developer("name2", "surname2", 200);
		Employee developer3 = new Developer("name3", "surname3", 300);

		Employee manager = new Manager("Hieu", "Ngo", 800, 100);
		Employee manager1 = new Manager("Hieu1", "Ngo1", 800, 100);
		Employee manager23 = new Manager("Hieu23", "Ngo23", 800, 100);

		developer1.setAddress("country1", "city1", "street1", 100);
		developer2.setAddress("country2", "city2", "street2", 200);
		developer3.setAddress("country3", "city3", "street3", 300);

		manager.setAddress("Poland", "Wroclaw", "Adama Mickiewicza", 98);
		manager1.setAddress("Poland1", "Wroclaw1", "Adama Mickiewicza1", 98);
		manager23.setAddress("Poland23", "Wroclaw23", "Adama Mickiewicza23", 98);

		((Manager) manager1).addSubordinates((Developer) developer1);
		((Manager) manager23).addSubordinates((Developer) developer2);
		((Manager) manager23).addSubordinates((Developer) developer3);

		System.out.println(developer1.toString());
		System.out.println(manager.toString());
		System.out.println(manager1.toString());
		System.out.println(manager23.toString());
	}
}
