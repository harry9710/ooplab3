package ooplab3;

import java.util.ArrayList;
import java.util.List;

public class Manager extends Employee {
	private int bonus;
	private List<Developer> subordinates = new ArrayList<Developer>();

	// constructor
	public Manager(String name, String surname, int salary, int bonus) {
		super(name, surname, salary);
		this.bonus = bonus;
	}

	// method to add developer as a subordinate of manager
	public void addSubordinates(Developer developer) {
		subordinates.add(developer);
	}

	@Override // method to calculate salary
	public int getSalary() {
		return super.getSalary() + this.bonus;
	}

	@Override // method to print basic information
	public String toString() {
		String subordinatesList = "";
		if (subordinates != null) {
			for (Developer developer : subordinates) {
				subordinatesList += "Subordinate " + (developer).toString();
			}
		}
		return super.toString() + subordinatesList;
	}
}
